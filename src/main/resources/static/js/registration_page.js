
$(document).ready(function() {
   $(".add").click(function(e){
   e.preventDefault();   
   var family_member_id = parseInt(document.getElementById("familyCount").value);  
   var imageDataKey = "ImageData_" + family_member_id;
   var nameKey = "name_" + family_member_id;
   var name = document.getElementById(nameKey).value;
   var imageData = document.getElementById(imageDataKey).value;
   if(name=='' || imageData==''){
	   alert("Kindly fill all details and capture photograph for family member, then add another family member");
	   return false;
   }else{
   family_member_id = family_member_id + 1;   
   var elem = '<div class="col-12 bor-top"></div>  <div class="col-md-12"> <h4>Family Member-'+family_member_id+' Details </h4> </div> <div class="col-md-6"><div class="form-group"><div class="form-group"> ' +
	          '<label for="name">Name</label><input type="text" class="form-control" name="name[]" id="name_' + family_member_id  + '" maxlength=80 required /></div></div></div>'+
	          '<div class="col-md-6"><div class="form-group"><label class="font-weight-bold">Gender</label><br/><div class="custom-control custom-radio custom-control-inline">' +
	          '<input type="radio" id="gender_male_' + family_member_id + '"  name="gender_radio_' + family_member_id + '" class="custom-control-input" value="Male" required><label class="custom-control-label" ' +
	          'for="gender_male_' + family_member_id + '">Male</label></div><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="gender_female_' + family_member_id + '" name="gender_radio_' + family_member_id + '"'+
	          'class="custom-control-input" value="Female" required><label class="custom-control-label" for="gender_female_' + family_member_id + '">Female</label></div></div></div><div class="col-md-6"><div class="form-group"><label for="dob">DOB:</label><input type="date" class="form-control" name="dob[]" id="dob_' + family_member_id + '" required />'+
	          '</div></div><div class="col-md-6"><div class="form-group"><label for="relation">Patient Relation:</label><input type="text" class="form-control" name="relation[]" id="relation_' + family_member_id + '" required /></div>'+
	          '</div><div class="col-md-6"><label class="font-weight-bold">Symptoms</label><br/><div class="custom-control custom-checkbox custom-control-inline"><input type="checkbox" class="custom-control-input" id="none_' + family_member_id + '" name="symptoms_' + family_member_id  + '" value="None" onClick="symptomsCheckBoxDisable(this)">'+
	          '<label class="custom-control-label" for="none_' + family_member_id + '">None</label></div><div class="custom-control custom-checkbox custom-control-inline"><input type="checkbox" class="custom-control-input" value="Fever" id="fever_' + family_member_id + '" name="symptoms_' + family_member_id  + '" onClick="symptomsCheckBoxEnable(this,none_' + family_member_id  + ')">'+
	          '<label class="custom-control-label" for="fever_' + family_member_id + '">Fever</label></div><div class="custom-control custom-checkbox custom-control-inline"><input type="checkbox" class="custom-control-input" id="hbp_' + family_member_id + '" name="symptoms_' + family_member_id  + '" onClick="symptomsCheckBoxEnable(this,none_' + family_member_id  + ')" '+
	          'value="Headache and Body Pains"><label class="custom-control-label" for="hbp_' + family_member_id + '">Headache and Body Pains</label></div><div class="custom-control custom-checkbox custom-control-inline"><input type="checkbox" class="custom-control-input" id="cough_' + family_member_id + '" name="symptoms_' + family_member_id  + '" onClick="symptomsCheckBoxEnable(this,none_' + family_member_id  + ')" '+
	          'value="Cough"><label class="custom-control-label" for="cough_' + family_member_id + '">Cough</label></div><div class="custom-control custom-checkbox custom-control-inline"><input type="checkbox" class="custom-control-input" id="sore_throat_' + family_member_id + '" name="symptoms_' + family_member_id  + '" onClick="symptomsCheckBoxEnable(this,none_' + family_member_id  + ')" '+
	          'value="Sore Throat"><label class="custom-control-label" for="sore_throat_' + family_member_id + '">Sore Throat</label></div><div class="custom-control custom-checkbox custom-control-inline"><input type="checkbox" class="custom-control-input" id="sob_' + family_member_id + '" name="symptoms_' + family_member_id  + '" onClick="symptomsCheckBoxEnable(this,none_' + family_member_id  + ')" '+
	          'value="Shortness of Breath"><label class="custom-control-label" for="sob_' + family_member_id + '">Shortness of Breath</label></div></div> <div class="col-md-6"><div class="form-group"></div></div> <div class="col-md-6 last_col6"> <div class="form-group"><canvas id="canvas_' + family_member_id + '" width="1000" height=1000 style="border: 1px solid #c0c0c0; height:250px;"></canvas> <textarea style="display:none;" name="ImageData_' + family_member_id + '" id="ImageData_' + family_member_id + '"></textarea> </div></div>';
      $(elem).insertAfter($('.last_col6').last());
      document.getElementById("familyCount").value =  family_member_id;
   }
     });
   
  
  
/*  $(document).on('click', '.dob', function(){
	  $(this).datetimepicker({
			timepicker:false,
			format:"d-m-Y",
			maxDate:0,
			yearStart: '1920',
			yearEnd: 2020
	  }).focus();
  });*/
   
  /* $(document).on('change', 'input[name="symptoms_1"]', function(){
	   var sel_sym = $(this).val();
	   alert("in checkbox="+sel_sym);
       if ($('#none_1').is(':checked')) {
    	   alert("none checkbox checked");
             $(this).parent().nextAll().find('input').attr("disabled",true);
       }
       else{
             $(this).parent().nextAll().find('input').attr("disabled",false);
       }
   });*/
  
  $("#covid_patient_form").submit(function (event) {
      event.preventDefault();
      //$(this).find(':submit').attr( 'disabled','disabled' );
      fire_ajax_submit();
  });
  
  if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
	    alert("Geolocation is not supported by this browser.");
  }
  
 

  function showPosition(position) {
	  $("#latitude").val(position.coords.latitude);
	  $("#longitude").val(position.coords.longitude);
  }
  
  function fire_ajax_submit() {
	  var spinner = $('#loader');
		
	  var patient_id = document.getElementById("patient_id").value;
	  var police_station = document.getElementById("police_station").value;
	  var email = document.getElementById("email").value;
	  var contact_number = document.getElementById("contact_number").value;
	  var house_no = document.getElementById("house_no").value;
	  var street = document.getElementById("street").value;
	  var landmark_village = document.getElementById("landmark_village").value;
	  var district = $("#district").val();  
	  var state =  $("#state").val();  
	  var pincode = document.getElementById("pincode").value;
	  var familyCount = parseInt(document.getElementById("familyCount").value);
	  var patientAndFamilyMemberDetails = "";
	  var latitude = document.getElementById("latitude").value;
	  var longitude = document.getElementById("longitude").value;
	  
	  if(email!= '' && IsEmail(email)==false){
		  alert("Please enter valid email address")
          return false;
       }
	  /*if(contact_number!= '' && IsPhone(contact_number)==false){
		  alert("Please enter valid Contact Number")
          return false;
       }*/
	  if(contact_number!= '' && contact_number.length<10){
		  alert("Please enter valid Contact Number")
          return false;
		}
	  if(pincode!= '' && pincode.length<6){
		  alert("Please enter valid Pincode")
          return false;
		}
	  var patientDetailsMap = {};
	
	  for (i = 1; i <= familyCount; i++) {
		  var nameKey = "name_" + String(i);
		  var dobKey = "dob_" + String(i);
		  var genderKey = "gender_radio_" + String(i);
		  var symptomsKey = "symptoms_" +  String(i);
		  var relationKey= "relation_" + String(i);
		  var imageDataKey = "ImageData_" + String(i);
		  var name = document.getElementById(nameKey).value;
		  var dob = document.getElementById(dobKey).value;
		  var patient_relation = document.getElementById(relationKey).value;
		  var gender = $('input[name="' + genderKey +'"]:checked').val();
		  var imageData = document.getElementById(imageDataKey).value;
		  var values = "";
		  $.each($("input[name='" + symptomsKey + "']:checked"), function() {
			  if (values == "") {
				  values = values + $(this).val();
			  } else {
				  values = values + ";" + $(this).val();
			  }
		  });
		  patientDetailsMap[nameKey] = name;
		  patientDetailsMap[dobKey] = dob;
		  patientDetailsMap[genderKey] = gender;
		  patientDetailsMap[relationKey] = patient_relation;
		  patientDetailsMap[symptomsKey] = values;
		  patientDetailsMap[imageDataKey] = imageData;
	  }
	  
	  var data = {};
	  data.patient_id = patient_id;
	  data.police_station = police_station;
	  data.email = email;
	  data.contact_number = contact_number;
	  data.house_no = house_no;
	  data.street = street;
	  data.landmark_village = landmark_village;
	  data.district = district;
	  data.state = state;
	  data.pincode = pincode;
	  data.family_count = familyCount
	  data.patientDetailsMap = patientDetailsMap;
	  data.latitude = latitude;
	  data.longitude = longitude;
	  $(':button[type="submit"]').prop('disabled', true);
	  spinner.show();
	  $.ajax({
  	    url: 'patientandfamilydetails',
  	    type: 'POST',
  	    data: JSON.stringify(data),
  	    contentType: "application/json",
        cache: false,
        timeout: 600000,
        success: function (data) {
         if(!alert(data)){window.location.reload();}
         $(':button[type="submit"]').prop('disabled', false);
         spinner.hide();
        },
        error: function (e) {
         if(!alert("Something went wrong at server side.")) {
        	 window.location.reload();
        	 $(':button[type="submit"]').prop('disabled', false);
        	 spinner.hide();
         }
        }
  	});
	  
  }
 
var width = 1000;    
  var height = 1000;     
  var streaming = false;
  var video = null;
  var canvas = null;
  var photo = null;
  var startbutton = null;

  function startup() {
    video = document.getElementById('video');
    startbutton = document.getElementById('snap');
    
    navigator.mediaDevices.getUserMedia({video: true, audio: false})
    .then(function(stream) {
      video.srcObject = stream;
      video.play();
    })
    .catch(function(err) {
      console.log("An error occurred: " + err);
    });
    
    video.addEventListener('canplay', function(ev){
      if (!streaming) {
        height = video.videoHeight / (video.videoWidth/width);
        if (isNaN(height)) {
          height = width / (4/3);
        }
        video.setAttribute('width', width);
        video.setAttribute('height', height);
        streaming = true;
      }
    }, false);
    
    startbutton.addEventListener('click', function(ev){
        takepicture();
        ev.preventDefault();
      }, false);
  }


 
  function takepicture() {
	  var family_member_id = parseInt(document.getElementById("familyCount").value);				
	  var canvasKey = "canvas_" + family_member_id;
	  var imageDataKey = "ImageData_" + family_member_id;
	  var canvas = document.getElementById(canvasKey);
      var context = canvas.getContext('2d');
      context.drawImage(video, 0, 0, 1000, 1000);
      var ImageData=canvas.toDataURL("image/jpeg");
      document.getElementById(imageDataKey).value= ImageData;
   
  }
  
  startup();
  
}); 
 
$(window).on('beforeunload', function(){
    $(window).scrollTop(0);
});

function IsEmail(email) {
	  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  if(!regex.test(email)) {
	    return false;
	  }else{
	    return true;
	  }
	}

function IsPhone(phone) {
	var regex = /^\d{10}$/;
	if(phone.value.match(regex))
	 {
	      return true;
	 } else{
	        return false;
	  }
	}

function numbersonly_mobile(f,b,d){var a;var c;if(window.event){a=window.event.keyCode;}else{if(b){a=b.which;}else{return true;}}c=String.fromCharCode(a);if((a==null)||(a==0)||(a==8)||(a==9)||(a==13)||(a==27)){return true;}else{if((("0123456789").indexOf(c)>-1)){return true;}else{if(d&&(c==".")){f.form.elements[d].focus();return false;}else{return false;}}}}
function numericsonly_mobile(f,b,d){var a;var c;if(window.event){a=window.event.keyCode;}else{if(b){a=b.which;}else{return true;}}c=String.fromCharCode(a);if((a==null)||(a==0)||(a==8)||(a==9)||(a==13)||(a==27)){return true;}else{if((("0123456789.").indexOf(c)>-1)){return true;}else{if(d&&(c==".")){f.form.elements[d].focus();return false;}else{return false;}}}}
function charactersonly_mobile(f,b,d){var a;var c;if(window.event){a=window.event.keyCode;}else{if(b){a=b.which;}else{return true;}}c=String.fromCharCode(a);if((a==null)||(a==0)||(a==8)||(a==9)||(a==13)||(a==27)||(a==32)){return true;}else{if((("abcdefghijklmnopqrstuvwxyz").indexOf(c)>-1)){return true;}else{if((("ABCDEFGHIJKLMNOPQRSTUVWXYZ").indexOf(c)>-1)){return true;}else{if(d&&(c==".")){f.form.elements[d].focus();return true;}else{return false;}}}}}
function charsAndNumbersOnly(f,b,d){var a;var c;if(window.event){a=window.event.keyCode;}else{if(b){a=b.which;}else{return true;}}c=String.fromCharCode(a);if((a==null)||(a==0)||(a==8)||(a==9)||(a==13)||(a==27)||(a==32)){return true;}else{if((("abcdefghijklmnopqrstuvwxyz1234567890.").indexOf(c)>-1)){return true;}else{if((("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.").indexOf(c)>-1)){return true;}else{if(d&&(c==".")){f.form.elements[d].focus();return true;}else{return false;}}}}}function charsNumbersAndSpecialCharsOnly(f,b,d){var a;var c;if(window.event){a=window.event.keyCode;}else{if(b){a=b.which;}else{return true;}}c=String.fromCharCode(a);if((a==null)||(a==0)||(a==8)||(a==9)||(a==13)||(a==27)||(a==32)){return true;}else{if((("abcdefghijklmnopqrstuvwxyz1234567890.,()&-_/':;").indexOf(c)>-1)){return true;}else{if((("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.,()&-_/:;").indexOf(c)>-1)){return true;}else{if(d&&(c==".")){f.form.elements[d].focus();return true;}else{return false;}}}}}


function symptomsCheckBoxDisable(symptoms){
	 if($(symptoms).prop("checked") == true){ 
	    $(symptoms).parent().nextAll().find('input').attr("disabled",true);
	 }else{
		 $(symptoms).parent().nextAll().find('input').attr("disabled",false); 
	 }
 }
 
 function symptomsCheckBoxEnable(symptoms,none){
	   $(none).attr("disabled",true);
	   
 }
