package com.tgreen.covid19.controller;

import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	private static Logger log = Logger.getLogger("HomeController.class");
	
	@RequestMapping(value = { "/", "login", "home" })
	public String home(Model model) {
		return "registration_page";
	}
	
}
