package com.tgreen.covid19.controller;

import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgreen.covid19.model.Patientdto;
import com.tgreen.covid19.service.COVIDPatientService;

@RestController
public class Covid19RestController {

	private static Logger log = Logger.getLogger("CandidateRestController.class");

	@Autowired
	private COVIDPatientService covidPatientService;

	@RequestMapping(path = "/patientandfamilydetails", method = RequestMethod.POST)
	public ResponseEntity<String> submitPatientAndFamilyDetails(@Valid @RequestBody Patientdto patientdto) {
		log.info("patientid: " + patientdto.getPatient_id());
		String msg = covidPatientService.savePatientAndFamilyDetails(patientdto);
		return ResponseEntity.ok(msg);
	}

}
