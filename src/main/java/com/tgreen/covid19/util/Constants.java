package com.tgreen.covid19.util;

public interface Constants {

	String INSTALLATION_ISSUE = "Python Installation Issue.";
	String OS_NAME = "os.name";
	String WIN_OS = "Windows 10";
	String WIN_SEPERATOR = "\\";
	String UBUNTU_SEPERATOR = "/";
	String IMAGES_NOT_SAVED_ERROR = "Images are not saved on disk, please check permissions.";
	
	String WINDOWSPATH = "C:\\AdmitcardPhotoVerification\\";
	String UBUNTUPATH = "AdmitcardPhotoVerificationImages/";
	String IMAGE1_EXT = "_1.png";
	String IMAGE2_EXT = "_2.png";
	
	String MATCHED_MSG = "Images Are Matched";
	String UNMATCHED_MSG = "Images Are Unmatched";
	String PHOTO_UNCLEAR_MSG = "Please Take A Clear Photo";
	
	String MATCHED = "matched";
	String UNMATCHED = "unmatched";
	String UNCLEARPHOTO = "Please take a clear pitcure";
	String PYTHON_CMD_ERROR = "Python Command Execution Error";
}
