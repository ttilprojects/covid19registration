package com.tgreen.covid19.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.tgreen.covid19.model.HistoryUpdates;

@Repository
public interface HistoryUpdatesRepository extends PagingAndSortingRepository<HistoryUpdates, Long> {
	
}
