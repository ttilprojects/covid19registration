package com.tgreen.covid19.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.tgreen.covid19.model.Patient;

@Repository
public interface PatientRepository extends PagingAndSortingRepository<Patient, Long> {
	
	@Query("Select p from Patient p where p.patientId=?1")
	Patient findPatientByPatientId(String patientId);
	
}
