package com.tgreen.covid19.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.tgreen.covid19.model.Base64ImageDto;
import com.tgreen.covid19.model.Patient;
import com.tgreen.covid19.model.PatientFamily;
import com.tgreen.covid19.model.Patientdto;
import com.tgreen.covid19.repository.PatientFamilyRepository;
import com.tgreen.covid19.repository.PatientRepository;

@Service
public class COVIDPatientService {
	
	private static Logger log = Logger.getLogger("COVIDPatientService.class");
	
	@Value("${python.script.path}")
	private String pythonScriptCommand;
	
	@Autowired
	private PatientRepository patientRepository;

	@Autowired
	private PatientFamilyRepository patientFamilyRepository;
	
	@Autowired
	private AWSService awsService;
	
	@Transactional
	public String savePatientAndFamilyDetails(Patientdto patientdto) {
		System.out.println("in controller");
		Patient patient1 = patientRepository.findPatientByPatientId(patientdto.getPatient_id().trim());
		if (patient1 != null) {
			return "Patient already exists in the database.";
		}
		Patient patient = new Patient();
		patient.setPatientId(patientdto.getPatient_id());
		patient.setNearPolice(patientdto.getPolice_station());
		patient.setEmail(patientdto.getEmail());
		patient.setContactNumber(patientdto.getContact_number());
		patient.setHno(patientdto.getHouse_no());
		patient.setStreet(patientdto.getStreet());
		patient.setVillage(patientdto.getLandmark_village());
		patient.setDistrict(patientdto.getDistrict());
		patient.setState(patientdto.getState());
		patient.setPincode(patientdto.getPincode());
		patient.setName(patientdto.getPatientDetailsMap().get("name_1"));
		patient.setCreated_at(new Date());
		patient.setLattitude(patientdto.getLatitude());
		patient.setLongitude(patientdto.getLongitude());
		List<PatientFamily> patientFamilList = new ArrayList<PatientFamily>();
		int count = Integer.parseInt(patientdto.getFamily_count());
		for (int i=1; i <= count; i++) {
			PatientFamily patientFamily = new PatientFamily();
			patientFamily.setPatientId(patientdto.getPatient_id());
			patientFamily.setName(patientdto.getPatientDetailsMap().get("name_" + i));
			patientFamily.setGender(patientdto.getPatientDetailsMap().get("gender_radio_" + i));
			patientFamily.setRelationPatient(patientdto.getPatientDetailsMap().get("relation_" + i));
			try {
				//patientFamily.setDob(new SimpleDateFormat("dd-MM-yyyy").parse(patientdto.getPatientDetailsMap().get("dob_" + i)));
				patientFamily.setDob(new SimpleDateFormat("yyyy-MM-dd").parse(patientdto.getPatientDetailsMap().get("dob_" + i)));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			String symptoms = patientdto.getPatientDetailsMap().get("symptoms_" + i);
			StringTokenizer strToken = new StringTokenizer(symptoms, ";");
			while (strToken.hasMoreTokens()) {
				switch (strToken.nextElement().toString()) {
				case "Fever":
					patientFamily.setS1Fever(true);
				    break;
				case "Headache and Body Pains":
					patientFamily.setS2Headache(true);
				    break;
				case "Cough":
					patientFamily.setS3Cough(true);
				    break;
				case "Sore Throat":
					patientFamily.setS4Sorethroat(true);
				    break;
				case "Shortness of Breath":
					patientFamily.setS5Breathlesness(true);
				    break;
				case "None":
					patientFamily.setS6None(true);
					break;
				}
			}
			patientFamily.setCreated_at(new Date());
		
			//AmazonS3 s3 = new AmazonS3Client(AWSCredintialsConfig.getAWSCredentials());
			//uploadImageS3Bucket(patientdto.getPatientDetailsMap().get("ImageData_" + i),s3,patientdto.getPatient_id(),i+"");
			uploadImagesIntoS3Bucket(patientdto.getPatientDetailsMap().get("ImageData_" + i),patientdto.getPatient_id(),i+"");
			patientFamily.setPhotoLink("http://punjabvocid19mgmt.s3.amazonaws.com/registration/"+patientdto.getPatient_id()+"_"+i+".jpg");
			patientFamilList.add(patientFamily);
		}
		patientRepository.save(patient);
		patientFamilyRepository.saveAll(patientFamilList);
		return "Patient Details are submitted successfully";
	}
	
	public void uploadImageS3Bucket(String imageString,AmazonS3 s3,String patientId,String imagename ) {
		try {
			if(imageString!=null){
					byte decoded[] = new sun.misc.BASE64Decoder().decodeBuffer(imageString.replaceAll("data:image/jpeg;base64,", ""));
					InputStream in = new ByteArrayInputStream(decoded);
					s3.putObject(new PutObjectRequest("punjabvocid19mgmt",patientId+"_"+imagename+".jpg",in, new ObjectMetadata()));

			}
			//bis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void uploadImagesIntoS3Bucket(String patient_1_photo,String patientId,String imagename) {
		Base64ImageDto base64ImageDto = new Base64ImageDto(patient_1_photo, patientId+"_"+imagename+".jpg");
		awsService.uploadBase64Image(base64ImageDto, "","registration");
	}
	
}
