package com.tgreen.covid19.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.tgreen.covid19.model.Base64ImageDto;

@Service
public class AWSService {

	private static Logger log = Logger.getLogger("AWSService.class");
	
	@Value("${aws.accesskey}")
	private String ACCESSKEY;
	
	@Value("${aws.secretkey}")
	private String SECRETKEY;

	@Value("${aws.region}")
	private String awsRegion;
	
	@Value("${aws.bucket}")
	private String bucket;
	
	@Value("${aws.key.prefix.candidate}")
	private String key_prefix_candidate;
	
	@Value("${aws.key.prefix.admitcard}")
	private String key_prefix_admitcard;
	
	@Value("${aws.key.suffix.candidate}")
	private String key_suffix_candidate;
	
	@Value("${aws.key.suffix.admitcard}")
	private String key_suffix_admitcard;
	
	//TransferManager s3TransferManager = TransferManagerBuilder.standard().build();

	private AmazonS3 getClient() {
		final AWSCredentialsProvider credentials = new AWSStaticCredentialsProvider(new BasicAWSCredentials(ACCESSKEY, SECRETKEY));
		return AmazonS3ClientBuilder.standard().withCredentials(credentials).withRegion(awsRegion).build();
	}

	public String getFileStream(String fileName, int type) {
		String key;
		if (type == 1) {
			key = key_prefix_candidate + fileName + key_suffix_candidate;
		} else {
			key = key_prefix_admitcard + fileName + key_suffix_admitcard;
		}
		AmazonS3 s3Client = getClient();
		GetObjectRequest getRequest = new GetObjectRequest(bucket, key);
		S3Object result = s3Client.getObject(getRequest);
		InputStream inputStream = result.getObjectContent();
		byte[] bytes = null;
		try {
			bytes = IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Base64.getEncoder().encodeToString(bytes);
	}
	
	public void uploadBase64Image(Base64ImageDto base64ImageDto, String pathToFile,String folderName) {
		
		final AmazonS3 s3 = getClient();
	    final TransferManager s3TransferManager = TransferManagerBuilder.standard().withS3Client(s3).build();
	    
	    InputStream stream = new ByteArrayInputStream(base64ImageDto.getImageBytes());

	    ObjectMetadata metadata = new ObjectMetadata();
	    metadata.setContentLength(base64ImageDto.getImageBytes().length);
	    metadata.setContentType("image/"+base64ImageDto.getFileType());

	    String bucketName = bucket;
	    //String key = pathToFile + base64ImageDto.getFileName();
	    String key = base64ImageDto.getFileName();

	    try {
	    	log.info("Uploading file " + base64ImageDto.getFileName() + " to AWS S3");
	        PutObjectRequest objectRequest = new PutObjectRequest(bucketName, folderName+"/"+key, stream, metadata);
	        objectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
	        Upload s3FileUpload = s3TransferManager.upload(objectRequest);
	        s3FileUpload.waitForCompletion();
	    } catch (Exception e) {
	        e.printStackTrace();
	        log.info("Error uploading file " + base64ImageDto.getFileName() + " to AWS S3");
	    }      
	}
	
}
