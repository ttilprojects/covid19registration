package com.tgreen.covid19.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "patient")
public class Patient implements Serializable {

	private static final long serialVersionUID = 2794603836204121037L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, updatable = false, nullable = false)
	private Long id;
	
	@Column(name = "patient_id")
	private String patientId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "contact_number")
	private String contactNumber;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "near_police")
	private String nearPolice;
	
	@Column(name = "hno")
	private String hno;
	
	@Column(name = "street")
	private String street;
	
	@Column(name = "village")
	private String village;
	
	@Column(name = "tehsil")
	private String tehsil;
	
	@Column(name = "district")
	private String district;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "pincode")
	private String pincode;
	
	@Column(name = "ipadress")
	private String ipadress;
	
	@Column(name = "created_at", columnDefinition = "DATETIME")
	private Date created_at;
	
	@Column(name = "lattitude")
	private String lattitude;
	
	@Column(name = "longitude")
	private String longitude;
	
	@Column(name = "browser")
	private String browser;
	
}
