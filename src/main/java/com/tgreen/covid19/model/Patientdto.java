package com.tgreen.covid19.model;

import java.util.Map;

import lombok.Data;

@Data
public class Patientdto {
	
	private String patient_id;
	private String police_station;
	private String email;
	private String contact_number;
	private String house_no;
	private String street;
	private String landmark_village;
	private String district;
	private String state;
	private String pincode;
	private String family_count;
	private String latitude;
	private String longitude;
	private Map<String, String> patientDetailsMap;
	private String patient_1_photo;
	
}
