package com.tgreen.covid19.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lombok.Data;

@Data
@Entity
@Table(name = "history_updates")
public class HistoryUpdates implements Serializable {

	private static final long serialVersionUID = 846825753414895959L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, updatable = false, nullable = false)
	private Long id;
	
	@Column(name = "sms_id")
	private Long smsId;
	
	@Column(name = "patient_id")
	private Long patientId;
	
	@Column(name = "family_id")
	private Long familyId;
	
	@Column(name = "photo_link")
	private String photoLink;
	
	@Column(name = "s1_fever")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean s1Fever;
	
	@Column(name = "s2_headache")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean s2Headache;
	
	@Column(name = "s3_cough")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean s3Cough;
	
	@Column(name = "s4_sorethroat")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean s4Sorethroat;
	
	@Column(name = "s5_breathlesness")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean s5Breathlesness;
	
	@Column(name = "s6_none")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean s6None;
	
	@Column(name = "browser")
	private String browser;
	
	@Column(name = "created_at", columnDefinition = "DATETIME")
	private Date created_at;
	
}
