package com.tgreen.covid19.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lombok.Data;

@Data
@Entity
@Table(name = "sms")
public class SMS implements Serializable {

	private static final long serialVersionUID = 846825753414895959L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sms_id", unique = true, updatable = false, nullable = false)
	private Long smsId;
	
	@Column(name = "patient_id")
	private Long patientId;
	
	@Column(name = "sms_slot")
	private Long smsSlot;
	
	@Column(name = "sms_content")
	private String smsContent;
	
	@Column(name = "created_at", columnDefinition = "DATETIME")
	private Date createdAt;
	
	@Column(name = "updated_at", columnDefinition = "DATETIME")
	private Date updatedAt;
	
	@Column(name = "sms_sent_at", columnDefinition = "DATETIME")
	private Date smsSentAt;
	
}
